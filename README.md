# Telegram logger

This is a proof-of-concept code that retrieves messages from Telegram and stores them in a database.

## Getting started

To begin, you'll need a Telegram API key. You can obtain one here:
https://core.telegram.org/api/obtaining_api_id

## Dependencies

Install the necessary packages using the following command:
`pip3 install telethon sqlite3`

## Run the code

The first time you run the code, it will prompt you for your phone number or bot token. You'll then receive a 2FA code in your Telegram app chat. Copy and paste this code into your terminal.
It's highly recommended to run the code initially with the --view flag. This allows you to obtain the channel/group ID. Subsequently, you can use this specific ID to fetch messages into the SQLite database.

## Caution

To avoid being banned from Telegram, index only one channel/group at a time.

## ToDo

- Migrate from SQLite3 to MySQL, PostgreSQL, or Elasticsearch for improved database management.
- Develop a front-end interface for easier interaction and data visualization.
