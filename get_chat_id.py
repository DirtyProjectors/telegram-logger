from telethon import TelegramClient, sync

# Create your own API credential on Telegram website.
api_id = ''
api_hash = ''
session_name = 'chats_telegram'

client = TelegramClient(session_name, api_id, api_hash)

async def list_groups():
    async for dialog in client.iter_dialogs():
        if dialog.is_group or dialog.is_channel:
            print(f'Nombre: {dialog.name}, ID: {dialog.id}')

with client:
    client.loop.run_until_complete(list_groups())
