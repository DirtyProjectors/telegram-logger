from telethon import TelegramClient, events, sync, types
from telethon.tl.functions.messages import GetHistoryRequest
from datetime import datetime
import sqlite3
import asyncio
import hashlib
import argparse

api_id = ''
api_hash = ''
session_name = ''
client = TelegramClient(session_name, api_id, api_hash)

async def main(chat_id_arg, all_chats, view_only, verbose):
    # Listar grupos y canales
    group_channel_ids = []
    async for dialog in client.iter_dialogs():
        if dialog.is_group or dialog.is_channel:
            if view_only:
                print(f'Nombre: {dialog.name}, ID: {dialog.id}')
            elif all_chats or dialog.id == chat_id_arg:
                group_channel_ids.append(dialog.id)
    print("==============================")

    if view_only:
        return

    # Leer y guardar mensajes antiguos para cada grupo/canal
    for chat_id in group_channel_ids:
        chat = await client.get_entity(chat_id)
        # Convertir chat_id a una cadena y quitar el prefijo '-100'
        formatted_chat_id = int(str(chat_id)[4:])
        chat_name = chat.title  # O el atributo correspondiente
        username = getattr(chat, 'username', None)  # None si no está disponible
        creation_date = chat.date.strftime('%Y-%m-%d %H:%M:%S') if chat.date else None
        member_count = getattr(chat, 'participants_count', 0)
        megagroup = int(chat.megagroup) if hasattr(chat, 'megagroup') else 0
        broadcast = int(chat.broadcast) if hasattr(chat, 'broadcast') else 0

        if verbose:
            print(f"Procesando chat {chat_id}")

        # Otros campos que desees recopilar
        cursor.execute("INSERT OR REPLACE INTO group_info (chat_id, name, username, creation_date, member_count, megagroup, broadcast) VALUES (?, ?, ?, ?, ?, ?, ?)",
                   (formatted_chat_id, chat_name, username, creation_date, member_count, megagroup, broadcast))
        await save_old_messages(chat_id, verbose)
        await asyncio.sleep(2)  # Pequeña pausa para evitar límites de la API

conn = sqlite3.connect('telegram_messages.db')
cursor = conn.cursor()
cursor.execute('''CREATE TABLE IF NOT EXISTS messages 
                  (id INTEGER PRIMARY KEY, user_id INTEGER, 
                   username TEXT, content TEXT, date TEXT, 
                   chat_id INTEGER, capture_date TEXT, md5_hash TEXT)''')
cursor.execute('''CREATE TABLE IF NOT EXISTS group_info (
                  chat_id INTEGER PRIMARY KEY,
                  name TEXT,
                  username TEXT,
                  creation_date TEXT,
                  member_count INTEGER,
                  megagroup INTEGER,
                  broadcast INTEGER)''')

async def save_old_messages(chat_identifier, verbose):
    # Obtener la entidad del chat
    chat = await client.get_entity(chat_identifier)
    chat_name = chat.title  # Obtener el nombre del canal o grupo
    print(f"Extrayendo mensajes de: {chat_name} (ID: {chat.id})")
    
    # Ahora chat_id es una entidad, no solo un número o string
    total_messages = 0
    last_date = None
    chunk_size = 200
    while True:
        history = await client(GetHistoryRequest(
            peer=chat,
            offset_id=0,
            offset_date=last_date,
            add_offset=0,
            limit=chunk_size,
            max_id=0,
            min_id=0,
            hash=0
        ))
        if not history.messages:
            break # Salir del bucle si no hay más mensajes
        for message in history.messages:
            # Comprobar si message.from_id es un usuario y obtener user_id
            user_id = None
            if isinstance(message.from_id, types.PeerUser):
                user_id = message.from_id.user_id
            #else:
            #    user_id = None  # O puedes usar algún valor por defecto para los mensajes de canales
        #if not history.messages:
        #    break
        #for message in history.messages:
            # Guardar en la base de datos
            #user_id = message.from_id.user_id if message.from_id else None
            if message.message and not message.media:
                username = message.sender.username if message.sender else 'TL_unknown_user'
                content = message.message
                chat_id = chat.id
                capture_date = datetime.now().strftime('%Y-%m-%d %H:%M:%S')  # Fecha actual
                date = message.date.strftime('%Y-%m-%d %H:%M:%S')
                if message.message:
                    md5_hash = hashlib.md5(message.message.encode()).hexdigest()
                else:
                    md5_hash = None  # o asignar algún valor predeterminado
                cursor.execute("SELECT COUNT(*) FROM messages WHERE user_id = ? AND content = ? AND date = ? AND chat_id = ?",
                                (user_id, content, date, chat_id))
                if cursor.fetchone()[0] == 0:
                    # Si no hay registros existentes, insertar el nuevo registro
                    cursor.execute('INSERT INTO messages (user_id, username, content, date, chat_id, capture_date, md5_hash) VALUES (?, ?, ?, ?, ?, ?, ?)',
                                    (user_id, username, content, date, chat_id, capture_date, md5_hash))
                    conn.commit()
                #cursor.execute('INSERT INTO messages (user_id, username, content, date, chat_id, capture_date, md5_hash) VALUES (?, ?, ?, ?, ?, ?, ?)',
                #       (user_id, username, content, date, chat_id, capture_date, md5_hash))
                if verbose:
                    print(f"Guardando mensaje: {message.id} de grupo {chat_id} con fecha {date}")
            total_messages += 1
        last_date = min(msg.date for msg in history.messages)
    #conn.commit()
    print(f'Total de mensajes guardados: {total_messages}')

    """# Escuchar nuevos mensajes
    @client.on(events.NewMessage(chats=group_channel_ids))
    async def my_event_handler(event):
        sender = await event.get_sender()
        username = getattr(sender, 'username', 'Usuario Desconocido')
        user_id = sender.id
        content = event.message.message
        date = event.message.date.strftime('%Y-%m-%d %H:%M:%S')

        # Insertar en la base de datos
        cursor.execute('INSERT INTO messages (user_id, username, content, date) VALUES (?, ?, ?, ?)',
                       (user_id, username, content, date))
        conn.commit()"""

# Configuración de argparse
parser = argparse.ArgumentParser()
parser.add_argument('chat_id', nargs='?', type=int, help='ID del chat a procesar')
parser.add_argument('--all', action='store_true', help='Procesar todos los chats')
parser.add_argument('--view', action='store_true', help='Solo listar chats')
parser.add_argument('-v', '--verbose', action='store_true', help='Modo verboso para mostrar más información')
args = parser.parse_args()

with client:
    client.loop.run_until_complete(main(args.chat_id, args.all, args.view, args.verbose))
